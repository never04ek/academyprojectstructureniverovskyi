namespace AcademyProjectStructureNiverovskyi.Models
{
    public enum TaskState 
    {
        Created,
        Started,
        Finished,
        Canceled
    }
}