using Newtonsoft.Json;

namespace AcademyProjectStructureNiverovskyi.Models
{
    public abstract class Entity
    {
        [JsonProperty("id")] public int Id { get; set; }
    }
}