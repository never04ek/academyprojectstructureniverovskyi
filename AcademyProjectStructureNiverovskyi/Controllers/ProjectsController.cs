﻿using System.Collections.Generic;
using System.Linq;
using AcademyProjectStructureNiverovskyi.Models;
using AcademyProjectStructureNiverovskyi.Services;
using Microsoft.AspNetCore.Mvc;

namespace AcademyProjectStructureNiverovskyi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private readonly ProjectsService _projectsService;

        public ProjectsController(ProjectsService projectsService)
        {
            _projectsService = projectsService;
        }

        // GET api/projects
        [HttpGet]
        public ActionResult<IEnumerable<Project>> Get()
        {
            return Ok(_projectsService.GetProjects());
        }

        // GET api/projects/5
        [HttpGet("{id}")]
        public ActionResult<Project> Get(int id)
        {
            return Ok(_projectsService.GetProject(id));
        }

        // POST api/projects
        [HttpPost]
        public IActionResult Post([FromBody] Project value)
        {
            _projectsService.AddProject(value);
            return Ok(_projectsService.GetProjects().Count());
        }

        // PUT api/projects/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] Project value)
        {
            _projectsService.UpdateProject(id, value);
            return Ok(value);
        }

        // DELETE api/projects/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _projectsService.DeleteProject(id);
            return Ok();
        }
    }
}