using System.Collections.Generic;

namespace AcademyProjectStructureNiverovskyi.Repository
{
    public interface IRepository<T> where T : class
    {
        List<T> GetAll();
        void Add(T entity);
        void Delete(T entity);
        T Get(int id);
        void Update(T entity);
    }
}